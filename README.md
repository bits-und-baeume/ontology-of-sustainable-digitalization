# Ontology of Sustainable Digitalization (OSuDi)

This repo contains an experimental ontology^[1] which covers the intersection of sustainability^[2] and digitalization^[3]. To this end, it also covers concepts from both domains which are per se not part of that intersection. The intended application of this ontology is to serve as inventory for semantic tags for the *"Knowledge Base of Sustainable Digitalization"*.

## Current state

The ontology is in early stage of development and not officially released. The current taxonomy (hierarchy of sub-classes) is shown here:


[![taxonomy graph of osudi-prototype](./docs/osudi-prototype01.svg "taxonomy graph of osudi-prototype")](./docs/osudi-prototype01.svg)


## Goal

The aim of this project is to develop an multilingual ontology which contains all relevant concepts to semantically classify resources (documents, organizations, events, ...) in the field of sustainable digitalization. Whenever feasible it should reuse existing ontologies or other forms semantic knowledge description.

## Notes

For convenient manual development of the ontology it is currently represented in YAML. It can be automatically converted to rdf/xml with [yamlpyowl](https://github.com/cknoll/yamlpyowl):

```shell
yamlpyowl --convert-to-owl-rdf -o osusdig-prototype-01.owl osusdig-prototype-01.owl.yml
```

The result of this conversion is also included in this repo.


## Footnotes:

^[1]: An *Ontology* (in the sense of computer science) means roughly: a machine-processable specification of concepts and their relation, typically based on formal logics (so called description logics). This enables automatic reasoning e.g. that when someone is searching for items tagged with `ressources` and `africa` the results includes also items tagged with `coltan` and `congo`.

^[2]: *Sustainability* here means aiming for a societal framework which integrates global and intergenerational justice in terms of human rights and their ecological foundations.

^[3]: *Digitalization* (not to be confused with the related *digitalization*) refers to the process of using digital technologies in combination with other technologies in all areas of society (industry, private lives, government).

