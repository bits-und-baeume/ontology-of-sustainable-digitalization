# Ontologies with relevance to sustainability and digitalization

- Name: An Ontology-Based Knowledge Modelling for a Sustainability Assessment Domain
- IRI: http://www.semanticweb.org/aga/ontologies
- URL of Documents: https://mdpi-res.com/d_attachment/sustainability/sustainability-10-00300/article_deploy/sustainability-10-00300.pdf
- Date Publication: 24 January, 2018
- Link for owl files:
    - https://webprotege.stanford.edu/#projects/99888f55- c97d-471b-b992-bed90718b422
    - www.mdpi.com/2071-1050/10/2/300/s1
- Number of Concepts: 10

---

- Name: SDG Interface Ontology (SDGIO)
- IRI:  http://www.semanticweb.org/aga/ontologies/2017/9/untitled-ontology-9
- URL of Documents: https://www.unep.org/explore-topics/sustainable-development-goals/what-we-do/monitoring-progress/sdg-interface-ontology
- Date Publication: 13 November, 2017
- Link for owl files:
    - https://mdpi-res.com/d_attachment/sustainability/sustainability-10-00300/article_deploy/sustainability-10-00300-s001.zip?version=1516785769
- Number of Concepts: 10

---

- Name: Common Ontology of Sustainable Development
- IRI: http://www.semanticweb.org/boriana/ontologies/2018/4/untitled-ontology-6
- URL of Documents: https://www.igi-global.com/viewtitlesample.aspx?id=265531&ptid=229570&t=common+ontology+of+sustainable+development
- Date Publication: 1 December, 2020
- Link for owl files: https://github.com/Borydel/OSDE/blob/master/OSDE-last.owl
- Number of Concepts: 75

related:

- Name: Domain ontology of social sustainable development
- IRI: http://ontocom.net/downs/OntologySD.owl
- URL of Documents: https://aip.scitation.org/doi/10.1063/5.0042253
- Date Publication: 08 March 2021
- Link for owl files:
    - https://github.com/Borydel/OSSD
- Number of Concepts: 40

---

- Name: A core ontology for modeling life cycle sustainability assessment on the Semantic Web
- IRI: http://ontology.bonsai.uno/core
- URL of Documents: https://onlinelibrary.wiley.com/doi/epdf/10.1111/jiec.13220
- Date Publication: 25 December 2021
- Link for owl files:
    - https://ontology.bonsai.uno/core/ontology_v0.2.ttl
- Number of Concepts: 8

---

- Name: The ontology of the subject in digitalization
- IRI: https://www.datavillage.me/ontologies/LConsent
- URL of Documents: https://www.researchgate.net/publication/289080765_The_ontology_of_the_subject_in_digitalization
- Date Publication: 1 January 2012
- Link for owl files:
    - https://github.com/datavillage-me/ontologies/blob/master/consent/ontology.owl
- Number of Concepts: 5

---

- Name: Buddhist Digital Ontology (BDO)
- IRI: http://purl.bdrc.io/ontology/core/
- URL of Documents: https://github.com/buda-base/owl-schema https://www.mecs-press.org/ijitcs/ijitcs-v10-n12/IJITCS-V10-N12-1.pdf
- Date Publication: 9 March, 2021
- Link for owl files: https://github.com/buda-base/owl-schema/blob/master/core/bdo.ttl
- Number of Concepts: 5

---

- Name: Semantic, Digitization, Design and Implementation of Ontology in Social Internet-Services
- IRI: http://purl.bdrc.io/ontology/core/
- URL of Documents: http://ceur-ws.org/Vol-2588/paper20.pdf
- Date Publication: November 29, 2019
- Link for owl files: https://github.com/buda-base/owl-schema/blob/master/core/bdo.ttl
- Number of Concepts: 5

---

- Name: Onto-Digital: An Ontology-Based Model for Digital Transformation’s Knowledge
- IRI: http://localhost/concept/shopfloor
- URL of Documents: https://www.mecs-press.org/ijitcs/ijitcs-v10-n12/IJITCS-V10-N12-1.pdf
- Date Publication: 08 December, 2018
- Link for owl files: https://github.com/ko3n1g/Ontology-based-InformationFlow-Industry-4.0/tree/master/code/_ontology/data
- Number of Concepts: 10

---

- Name: Digitalization as a driver of transformative environmental innovation
- IRI: https://w3id.org/saref
- URL of Documents: https://www.sciencedirect.com/science/article/pii/S2210422421000733
- Date Publication: 01 December, 2021
- Link for owl files:
    - https://github.com/N5GEH/n5geh.datamodel
    - https://github.com/N5GEH/n5geh.datamodel/blob/master/Resources/Ontology-files/SARGON.owl
- Number of Concepts: 21

---

- Name: How to support digital sustainability assessment? An attempt to knowledge systematization
- IRI: http://www.google.com/digitalbuildings/0.0.1/hvac
- URL of Documents: Digital Buildings ontology
- Date Publication: https://pdf.sciencedirectassets.com/280203/1-s2.0-S1877050920X00147/1-s2.0-S1877050920321943/main.pdf
- Link for owl files: 01 May, 2021
- Number of Concepts:
    - https://github.com/protontypes/open-sustainable-technology
    - https://github.com/Azure/opendigitaltwins-smartcities
    - https://github.com/google/digitalbuildings/tree/master/ontology



---
- Name: The UDSA ontology: An ontology to support real time urban sustainability assessment
- IRI: http://www.usda.gov/dm/oaljdecisions
- URL of Documents: https://www.sciencedirect.com/science/article/abs/pii/S0965997818311773
- Date Publication: 01 February, 2020
- Link for owl files:
    - https://www.usda.gov/sites/default/files/documents/data.xml
- Number of Concepts: 30

---

- Name: An ontology for strongly sustainable business models: Defining an enterprise framework compatible with natural and social science
- IRI: http://purl.org/bmo/swot/1.0.0
- URL of Documents: https://www.sciencedirect.com/science/article/abs/pii/S0965997818311773
- Date Publication: 27 July, 2015
- Link for owl files:
    - https://github.com/batlinal/bmo/blob/master/swot.owl
- Number of Concepts: 12

---

- Name: The environment ontology in 2016: bridging domains with increased scope, semantic density, and interoperation
- IRI: http://www.environmentontology.org/
- URL of Documents: https://jbiomedsem.biomedcentral.com/articles/10.1186/s13326-016-0097-6
- Date Publication: 23 September, 2016
- Link for owl files:
    - https://sites.google.com/site/environmentontology/
- Number of Concepts: 29


---

- Name: Digital Ontology
- IRI: http://www.google.com/digitalbuildings/0.0.1/hvac
- URL of Documents: https://culanth.org/fieldsights/series/digital-ontology
- Date Publication: 31, October, 2020
- Link for owl files:
    - https://github.com/google/digitalbuildings/tree/master/ontology/rdf
- Number of Concepts: 15


---


- Name: Digital Existence
Ontology, Ethics and Transcendence in Digital Culture
- IRI: http://et.ims.su.se/
- URL of Documents: https://www.routledge.com/Digital-Existence-Ontology-Ethics-and-Transcendence-in-Digital-Culture/Lagerkvist/p/book/9780367588281#
- Date Publication: June 30, 2020
- Link for owl files:
    - https://github.com/google/digitalbuildings/blob/master/ontology/rdf/digital_buildings.owl
- Number of Concepts: 16


---

- Name: Consistent digitalization of engineering design – an ontology-based approach
- IRI: http://www.semanticweb.org/EII.owl#
- URL of Documents: https://www.designsociety.org/download-publication/40918/Consistent+digitalization+of+engineering+design+%E2%80%93+an+ontology-based+approach
- Date Publication: 17th August 2018
- Link for owl files:
    - https://www.designsociety.org/publication/40744
- Number of Concepts: 11

---


- Name: Ontology for digital entities
- IRI: http://purl.obolibrary.org/obo/envo.owl
- URL of Documents: https://www.lextechinstitute.ch/ontology-for-digital-entities/?lang=en
- Date Publication: 11 December 2013
- Link for owl files:
    - http://build.berkeleybop.org/job/build-envo/
- Number of Concepts: 14


---
---

## Non-Ontology Publications

- Name: Toward knowledge structuring of sustainability science based on ontology engineering
- URL: http://www.hozo.jp/
- URL of Documents: https://link.springer.com/article/10.1007/s11625-008-0063-z
- Date Publication: 10 February 2009
- Link for owl files:
    - http://iswc2011.semanticweb.org/fileadmin/iswc/Papers/Workshops/OCAS/99990029.pdf
- Number of Concepts: 17

---

- Name: Facilitating Digital Transformation by Multi-Aspect Ontologies: Approach and Application Steps
- URL: http://www-01.ibm.com/software/data/infosphere/
- URL of Documents: https://www.sciencedirect.com/science/article/pii/S2405896319314119
- Date Publication: 30 August, 2019
- Link for owl files:
    - http://www.aiai.ed.ac.uk/project/enterprise/enterprise/ontology.html
- Number of Concepts: 8

---

- Name: From numerical model to computational
intelligence: the digital transition of urban energy system
- URL: https://dr.ntu.edu.sg/
- URL of Documents: https://www.sciencedirect.com/science/article/pii/S1876610217365426
- Date Publication: 21 July, 2017
- Link for owl files:
    - https://github.com/Azure/opendigitaltwins-smartcities/blob/main/Ontology/Environment/environmentbasemodel.json
- Number of Concepts: 7


**Note**: This list was created by <https://www.fiverr.com/ronakpanchal105> based on a contract with the original committer.
